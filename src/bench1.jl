using Dynare

@time @eval @dynare "../models/model10", "-Dperiods=610";
@time @eval @dynare "../models/model20", "-Dperiods=300";
@time @eval @dynare "../models/model30", "-Dperiods=100";

## NonlinearSolve.jl
### Algorithm
```
Algorithm: TrustRegion(
   trustregion = GenericTrustRegionScheme(method = RadiusUpdateSchemes.NLsolve),
   descent = Dogleg(
    newton_descent = NewtonDescent(linsolve = LinearSolve.PardisoJL{Nothing, Nothing}
	      (nothing, nothing, nothing, false, nothing, nothing, nothing)), 
	steepest_descent = SteepestDescent(linsolve = LinearSolve.PardisoJL{Nothing, Nothing}
	(nothing, nothing, nothing, false, nothing, nothing, nothing)))
)
```
### Timings

```
using Dynare, Pardiso
@time begin; context = @dynare("modelXXa"); end;
```

#### 1st round
#### #model10, 610 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
364.590070 seconds (196.59 M allocations: 9.811 GiB, 1.16% gc time, 
                    87.46% compilation time)
```
##### model20, 300 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
1602.201469 seconds (442.11 M allocations: 20.802 GiB, 0.61% gc time, 
                     93.24% compilation time)
```

##### model30, 100 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
4678.882513 seconds (754.24 M allocations: 34.493 GiB, 1.05% gc time, 
                     96.78% compilation time)
```

#### 2nd round
##### model10, 610 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
 49.743339 seconds (19.96 M allocations: 2.115 GiB, 1.31% gc time)
```

##### model20, 300 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
114.529637 seconds (45.66 M allocations: 4.111 GiB, 0.97% gc time)
```

##### model30, 100 periods
```
 * Inf-norm of residuals: 0.000000
 * Convergence: Success
 * Function Calls (f): 5
169.394482 seconds (79.49 M allocations: 6.570 GiB, 12.51% gc time, 
                    0.00% compilation time)
```
Results of Nonlinear Solver Algorithm
 * Algorithm: TrustRegion(
   trustregion = GenericTrustRegionScheme(method = RadiusUpdateSchemes.NLsolve),
   descent = Dogleg(newton_descent = NewtonDescent(linsolve = LinearSolve.PardisoJL{Nothing, Nothing}(nothing, nothing, nothing, false, nothing, nothing, nothing)), steepest_descent = SteepestDescent(linsolve = LinearSolve.PardisoJL{Nothing, Nothing}(nothing, nothing, nothing, false, nothing, nothing, nothing)))
)
